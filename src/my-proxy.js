import console from './fake-console';

const myTarget = {
    name: 'David',
    age: 25
};

const myHandle = {
    get: function(target, prop){
        console.log(JSON.stringify(target), prop);
        return (prop in target) ? target[prop] : 'bad property!!!';
    },
    set: function(target, prop, value){
        console.log(JSON.stringify(target), prop, value);
        if(prop==='age' && isNaN(value/1)){
        } else {
            target[prop] = value;
        }
    },
}

const proxy = new Proxy(myTarget, myHandle);



const a = proxy.name;
const b = proxy.age;
const c = proxy.gender;

