export default class Person {
    constructor(name='unknown', age=NaN, gender='unknown') {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
    toString() {
        return `${this.name} ${this.age} ${this.gender}`;
    }
}