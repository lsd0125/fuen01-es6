import console from './fake-console';
import Person from './my-class01';

export default class Employee extends Person {
    constructor(name='unknown', age=NaN, gender='unknown') {
        super(name, age, gender);
        this._employeeId = '';
    }
    toString() {
        return `${this.name} - ${this.age} - ${this.gender} - ${this._employeeId}`;
    }
    get employeeId() {
        console.log('getter');
        return this._employeeId;
    }
    set employeeId(v){
        console.log('setter');
        this._employeeId = v;
    }
}