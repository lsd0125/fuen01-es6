import console from './fake-console';
import P from './my-class01';

const p1 = new P();
console.log(p1.toString());

const p2 = new P('Aaron', 29, 'male');
console.log(p2.toString());
console.log(p2.constructor.name);