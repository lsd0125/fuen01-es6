import console from './fake-console';

const ar = [5,8,3,{name:'bill', age: 28,},9];
const ar2 = [...ar]; // 淺層複製
const ar3 = JSON.parse(JSON.stringify(ar)); // 深層複製

ar[2] = 99;
ar[3].name = 'peter';

console.log(JSON.stringify(ar));
console.log(JSON.stringify(ar2));
console.log(JSON.stringify(ar3));

